import re
import os

with open(os.path.expanduser('~')+'/wynik1.txt','r') as  rf:
    with open(os.path.expanduser('~')+'/wynik2.txt', 'r') as  rf1:
        with open(os.path.expanduser('~')+'/wynik_excel.txt', 'w') as  wf:
            wf.write("base_link->odom\n")
            wf.write('Time            ,X     ,Y     ,Z     ,R     ,P     ,Y\n')
            for line in rf:
                match = re.search('At time (\d*\.?\d*)', line)
                match1 = re.search('- Translation: \[(.*?)\]', line)
                match2 = re.search('in RPY \(degree\) \[(.*?)\]', line)
                if match:
                    wf.write(match.group(1) + ", ")
                if match1:
                    wf.write(match1.group(1) + ", ")
                if match2:
                    wf.write(match2.group(1) + " \n")

            wf.write("\nsherlock_gt->odom\n")
            wf.write('Time            ,X     ,Y     ,Z     ,R     ,P     ,Y\n')
            for line in rf1:
                match3 = re.search('At time (\d*\.?\d*)', line)
                match4 = re.search('- Translation: \[(.*?)\]', line)
                match5 = re.search('in RPY \(degree\) \[(.*?)\]', line)
                if match3:
                    wf.write(match3.group(1) + ", ")
                if match4:
                    wf.write(match4.group(1) + ", ")
                if match5:
                    wf.write(match5.group(1) + " \n")
