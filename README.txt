### Installation: ###

PYLON SDK:

cd root_dir/sherlock/pylon_install
sudo tar -C /opt -xzf pylonSDK-5.0.5.9000-x86_64.tar.gz

SHERLOCK PACKAGE:

1) cd ~
2) mkdir -p sherlock_ws/src
3) cd sherlock_ws/src
4) catkin_init_workspace
5) git clone https://dkoguciuk@bitbucket.org/wutrobotics/sherlock.git (TUTAJ MUSISZ UZUPEŁNIĆ SWOIM LINKIEM Z BB)
6) cd ../
7) catkin_make
8) cd src
9) qtcreator CMakeLists.txt
10) ustaw folder ~/sherlock_ws/build jako miejsce budowania, byś mógł korzystać z CTRL+B

BUG przy QTcreator i CMake:
- otworzyć src/CMakeLists.txt, skopiować zawartość, usunąć ten plik, stworzyć nowy taki sam i wkleić tam tą zawartość


### Using: ###

CALIBRATION INTRINSICS:

1) Przygotuj wzorzec kalibracyjny
2) Edytuj plik: sherlock/config/calibration_1.xml (wpisz parametry wzorca oraz ustawienia kalibracji)
3) Jeśli ros master nie jest uruchomiony wpisz w konsoli: roscore
4) Otwórz nową konsolę i wpisz: source sherlock/devel/setup.bash
5) Upewnij się że kamera jest podłączona do komputera i włączona
6) Uruchom node camera_driver wpisując w konsoli: rosrun sherlock camera_driver _mode:=freerun (lub _mode:=triggered)
7) Otwórz kolejną konsolę i wpisz: source sherlock/devel/setup.bash
8) Uruchom node calibration_intrinsics wpisując: rosrun sherlock calibration_intrinsics
9) Ustaw wzorzec w polu widzenia kamery, naciśnij klawisz 's'
10) Wykonuj pionowe i poziome ruchy wzorcem w celu skalibrowania kamery
11) Naciśnij klawisz 'u' w celu podglądu obrazu bez dystorsji
12) Parametry kalibracji zostały zapisane do pliku sherlock/config/M1_data.xml

CALIBRATION EXTRINSICS:

1) Przygotuj wzorzec kalibracyjny kładąc go płasko na podłodze w polu widzenia kamery
2) Edytuj plik: sherlock/config/calibration_2.xml (wpisz parametry wzorca)
3) Jeśli ros master nie jest uruchomiony wpisz w konsoli: roscore
4) Otwórz nową konsolę i wpisz: source sherlock/devel/setup.bash
5) Upewnij się że kamera jest podłączona do komputera i włączona
6) Uruchom node camera_driver wpisując w konsoli: rosrun sherlock camera_driver _mode:=freerun (lub _mode:=triggered)
7) Otwórz kolejną konsolę i wpisz: source sherlock/devel/setup.bash
8) Uruchom node calibration_extrinsics wpisując: rosrun sherlock calibration_extrinsics
9) Połóż wzorzec płasko na podłodze w polu widzenia kamery, naciśnij klawisz 's'
10) Parametry kalibracji zostały zapisane do pliku sherlock/config/M2_data.xml

POSITION TRACKING:

1) Przygotuj robota do pracy, umieść na nim znacznik
2) Uruchom robota i przejedź nim w pole widzenia kamery
3) W celu połączenia systemu SherLock z robotem w środowisku ROS, otwórz plik .bashrc znajdujący się w katalogu domowym
4) Dodaj do pliku następujące komendy:
	- export ROS_IP= (IP twojego komputera w sieci) 	
	- export ROS_MASTER_URI=http://192.168.1.130:11311
	 (komputer musi być połączony z tą samą siecią wi-fi co robot)
5) Otwórz nową konsolę i wpisz: source sherlock/devel/setup.bash
6) Upewnij się że kamera jest podłączona do komputera i włączona
7) Uruchom node camera_driver wpisując w konsoli: rosrun sherlock camera_driver _mode:=freerun (lub _mode:=triggered)
8) Otwórz kolejną konsolę i wpisz: source sherlock/devel/setup.bash
9) Uruchom node position_tracking wpisując: rosrun sherlock position_tracking
10) Od teraz system SherLock śledzi robota
11) Pozycję robota według odometrii oraz według systemu SherLock można porównac w narzędziu rviz (w konsoli: rviz rviz)

ZAPIS DANYCH DO PLIKU:

1) Podczas działania programu position_tracking uruchom nową konsolę i wpisz :
     	rosrun tf tf_echo /batman/odom /batman/base_link 5 > base_link.txt
2) Następnie uruchom nową konsolę i wpisz kolejną komendę:
	rosrun tf tf_echo /batman/odom /batman/sherlock_gt 5 > sherlock_gt.txt
4) I po kilku sekundach przerwij działanie obydwu - ctrl+c
5) Utworzyło to dwa pliki z danymi wyjściowymi w katalogu domowym
6) Teraz uruchom skrypt do wyłuskania potrzebnych danych znajdujący się w katalogu /sherlock/scripts/data_extractor.py
7) Przejdź do tego katalogu i wpisz: python data_extractor.py
8) Utworzył się plik w katalogu domowym o nazwie "wyniki_excel.txt"
9) Teraz można ten plik zainportować do excela i porównać dane


