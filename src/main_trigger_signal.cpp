#include <string>
#include <time.h>
#include <fstream>
#include <iostream>
#include <sstream>
#include <unistd.h>
#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <signal.h>

// ROS includes
#include <ros/ros.h>

using namespace std;

//================================================================================
//================================= COLOR STRINGS ================================
//================================================================================

const std::string module_name = "trigger_signal";

const std::string red("\033[0;31m");
const std::string green("\033[1;32m");
const std::string yellow("\033[1;33m");
const std::string cyan("\033[0;36m");
const std::string magenta("\033[0;35m");
const std::string reset("\033[0m");

#define COUT_MODULE_NAME   green << "sherlock::" << module_name << " : " << reset
#define CERR_MODULE_NAME   "sherlock::" << module_name << " : "

    //============================================================================
    //=============================== GPIO CLASS =================================
    //============================================================================

class GPIOClass
{

public:

GPIOClass()
{
    this->gpionum = "4"; //GPIO4 is default
}

GPIOClass(string gnum)
{
    this->gpionum = gnum;  //Instatiate GPIOClass object for GPIO pin number "gnum"
}

int export_gpio()
{
    string export_str = "/sys/class/gpio/export";
    ofstream exportgpio(export_str.c_str()); // Open "export" file. Convert C++ string to C string. Required for all Linux pathnames

    exportgpio << this->gpionum ; //write GPIO number to export
    exportgpio.close(); //close export file
    return 0;
}

int unexport_gpio()
{
    string unexport_str = "/sys/class/gpio/unexport";
    ofstream unexportgpio(unexport_str.c_str()); //Open unexport file

    unexportgpio << this->gpionum ; //write GPIO number to unexport
    unexportgpio.close(); //close unexport file
    return 0;
}

int setdir_gpio(string dir)
{
    string setdir_str ="/sys/class/gpio/gpio" + this->gpionum + "/direction";
    ofstream setdirgpio(setdir_str.c_str()); // open direction file for gpio

    setdirgpio << dir; //write direction to direction file
    setdirgpio.close(); // close direction file
    return 0;
}

int setval_gpio(string val)
{
    string setval_str = "/sys/class/gpio/gpio" + this->gpionum + "/value";
    ofstream setvalgpio(setval_str.c_str()); // open value file for gpio

    setvalgpio << val ;//write value to value file
    setvalgpio.close();// close value file
    return 0;
}

int getval_gpio(string& val)
{
    string getval_str = "/sys/class/gpio/gpio" + this->gpionum + "/value";
    ifstream getvalgpio(getval_str.c_str());// open value file for gpio

    getvalgpio >> val ;  //read gpio value

    if(val != "0")
        val = "1";
    else
        val = "0";

    getvalgpio.close(); //close the value file
    return 0;
}

string get_gpionum()
{
    return this->gpionum;
}

private:
    string gpionum; // GPIO number associated with the instance of an object
};

bool ctrl_c_pressed = false;

void sig_handler(int sig)
{
    write(0,"nCtrl^C pressed in sig handlern",32);
    ctrl_c_pressed = true;
}

//--------------------------------------------------------MAIN-----------------------------------------------------------------

int main (void)
{
    struct sigaction sig_struct;
    sig_struct.sa_handler = sig_handler;
    sig_struct.sa_flags = 0;
    sigemptyset(&sig_struct.sa_mask);

    if (sigaction(SIGINT, &sig_struct, NULL) == -1)
    {
        cout << "Problem with sigaction" << endl;
        exit(1);
    }

    GPIOClass* gpio21 = new GPIOClass("21");

    gpio21->export_gpio();
    cout << " GPIO pins exported" << endl;

    gpio21->setdir_gpio("out");
    cout << " Set GPIO pin directions" << endl;

    while(1)
    {
        // 25 FPS
        gpio21->setval_gpio("1");
        usleep(4000);
        gpio21->setval_gpio("0");
        usleep(36000);
        cout << COUT_MODULE_NAME << "Signal was sent" << endl;

        if(ctrl_c_pressed)
        {
            cout << "Ctrl^C Pressed" << endl;
            cout << "unexporting pins" << endl;
            gpio21->unexport_gpio();
            cout << "deallocating GPIO Objects" << endl;
            delete gpio21;
            gpio21 = 0;
            break;
        }
    }
    cout << "Exiting....." << endl;
    return 0;
}
/*
    //============================================================================
    //=============================== NODE INIT ==================================
    //============================================================================

    ros::init(argc, argv, module_name);
    ros::NodeHandle n("~");
    ros::Rate rate(50); // ilość razy na sekundę

    //============================================================================
    //============================= PARAMS PARSING ===============================
    //============================================================================


    pub_image.publish(msg);     // Publikowanie wiadomości
    ros::spinOnce();            // Spin (by wysłać info z publishera do całego rosa
    rate.sleep();               // Czekamy, by wysyłać z opowiednią częstotliwością
*/

