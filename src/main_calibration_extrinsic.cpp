// standard
#include <iostream>
#include <iomanip>
#include <math.h>
#include <sstream>
#include <string.h>

// opencv
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/core.hpp>
#include <opencv2/core/utility.hpp>
#include <opencv2/imgproc.hpp>
#include <opencv2/calib3d.hpp>
#include <opencv2/imgcodecs.hpp>
#include <opencv2/videoio.hpp>
#include <opencv2/highgui.hpp>

// ROS includes
#include <ros/ros.h>
#include <cv_bridge/cv_bridge.h>
#include <image_transport/image_transport.h>

using namespace cv;
using namespace std;

enum { DETECTION = 0, CAPTURING = 1, CALIBRATED = 2 };

Mat sub_image;
vector<vector<Point2f> > image_points;
Mat camera_matrix, dist_coeffs, translation, rotation;
Size image_size;
int mode = DETECTION;
clock_t prevTimestamp = 0;
const Scalar RED(0,0,255), GREEN(0,255,0);
const char ESC_KEY = 27;

//================================================================================
//================================= COLOR STRINGS ================================
//================================================================================

const std::string module_name = "calibration_extrinsic";

const std::string red("\033[0;31m");
const std::string green("\033[1;32m");
const std::string yellow("\033[1;33m");
const std::string cyan("\033[0;36m");
const std::string magenta("\033[0;35m");
const std::string reset("\033[0m");

#define COUT_MODULE_NAME   green << "sherlock::" << module_name << " : " << reset
#define CERR_MODULE_NAME   "sherlock::" << module_name << " : "

//================================================================================
//=============================== SETTINGS CLASS ==============================
//================================================================================

class Settings
{
public:
    Settings() : goodInput(false) {}
    enum Pattern { NOT_EXISTING, CHESSBOARD, CIRCLES_GRID, ASYMMETRIC_CIRCLES_GRID};

    void write(FileStorage& fs) const
    {
        fs << "{"
                  << "BoardSize_Width"  << boardSize.width
                  << "BoardSize_Height" << boardSize.height
                  << "Square_Size"         << squareSize
                  << "Calibrate_Pattern" << patternToUse
                  << "Write_outputFileName"  << outputFileName
           << "}";
    }
    void read(const FileNode& node)
    {
        node["BoardSize_Width" ] >> boardSize.width;
        node["BoardSize_Height"] >> boardSize.height;
        node["Calibrate_Pattern"] >> patternToUse;
        node["Square_Size"]  >> squareSize;
        node["Write_outputFileName"] >> outputFileName;
        nrFrames = 1;
        validate();
    }
    void validate()
    {
        goodInput = true;
        if (boardSize.width <= 0 || boardSize.height <= 0)
        {
            cerr << "Invalid Board size: " << boardSize.width << " " << boardSize.height << endl;
            goodInput = false;
        }
        if (squareSize <= 10e-6)
        {
            cerr << "Invalid square size " << squareSize << endl;
            goodInput = false;
        }

        calibrationPattern = NOT_EXISTING;
        if (!patternToUse.compare("CHESSBOARD")) calibrationPattern = CHESSBOARD;
        if (!patternToUse.compare("CIRCLES_GRID")) calibrationPattern = CIRCLES_GRID;
        if (!patternToUse.compare("ASYMMETRIC_CIRCLES_GRID")) calibrationPattern = ASYMMETRIC_CIRCLES_GRID;
        if (calibrationPattern == NOT_EXISTING)
        {
            cerr << " Camera calibration mode does not exist: " << patternToUse << endl;
            goodInput = false;
        }
        atImageList = 0;
    }

public:
    Size boardSize;              // The size of the board -> Number of items by width and height
    Pattern calibrationPattern;  // One of the Chessboard, circles, or asymmetric circle pattern
    float squareSize;            // The size of a square in your defined unit (point, millimeter,etc).
    int nrFrames;                // The number of frames to use from the input for calibration
    string outputFileName;       // The name of the file where to write

    size_t atImageList;
    bool goodInput;

private:
    string patternToUse;

};

//================================================================================
//=============================== CALIBRATION CLASS ==============================
//================================================================================

class CalibrationExtrinsic
{

public:

    //============================================================================
    //============================= MAIN METHODS =================================
    //============================================================================

    /**
     * @brief CalibrationExtrinsic     The class constructor.
     * @param n                         Ros node handle object.
     */
    CalibrationExtrinsic(ros::NodeHandle& n, Settings& s)
    {
        // Get parent ns
        std::string ns = n.getNamespace();
        std::string parent_ns = ros::names::parentNamespace(ns);
        if (parent_ns == "/") image_topic = "/camera_driver/image";
        else image_topic = parent_ns + "/camera_driver/image";

        // Subscriber
        image_transport::ImageTransport it(n);
        sub = it.subscribe(image_topic, 10, &CalibrationExtrinsic::image_callback, this);

        // Timer
        timer = n.createTimer(ros::Duration(5), &CalibrationExtrinsic::timer_callback, this, true);

        // Haven't image yet
        got_image = false;

        calib_settings = s;
    }

    /**
     * @brief ~CalibrationExtrinsic    Destructor.
     */
    ~CalibrationExtrinsic()
    {
        timer.stop();
    }

    /**
     * @brief main_calib_function     Calibration Loop
     */
    void mainCalibFunction (Mat view, Settings& s)
    {

        bool blinkOutput = false;

       //-----  Jeśli brak obrazu lub pobrano wszystkie klatki to uruchom kalibracje -------------
       if( mode == CAPTURING && image_points.size() >= (size_t)s.nrFrames )
       {
         if( runCalibrationAndSave(s, image_size,  camera_matrix, dist_coeffs, image_points))
             mode = CALIBRATED;
         else
             mode = DETECTION;
       }

       image_size = view.size();  // Rozmiar pobranej klatki

       // -------------------------- Znajdź wzorzec --------------------------
       vector<Point2f> pointBuf;

       bool found;

       int chessBoardFlags = CALIB_CB_ADAPTIVE_THRESH | CALIB_CB_NORMALIZE_IMAGE | CALIB_CB_FAST_CHECK;

       switch( s.calibrationPattern ) // Znajdź charakterystyczne punkty na obrazie
       {
       case Settings::CHESSBOARD:
           found = findChessboardCorners( view, s.boardSize, pointBuf, chessBoardFlags);
           break;
       case Settings::CIRCLES_GRID:
           found = findCirclesGrid( view, s.boardSize, pointBuf );
           break;
       case Settings::ASYMMETRIC_CIRCLES_GRID:
           found = findCirclesGrid( view, s.boardSize, pointBuf, CALIB_CB_ASYMMETRIC_GRID );
           break;
       default:
           found = false;
           break;
       }

       // ------------------------ Jeśli znaleziono ------------------------------------
       if (found)
       {
               // Przejście na skalę szarności w celu lepszego wykrycia wzorca
               if( s.calibrationPattern == Settings::CHESSBOARD)
               {
                   Mat viewGray;
                   cvtColor(view, viewGray, COLOR_BGR2GRAY);
                   cornerSubPix( viewGray, pointBuf, Size(11,11),
                       Size(-1,-1), TermCriteria( TermCriteria::EPS+TermCriteria::COUNT, 30, 0.1 ));
               }

               // Zapamiętaj wykryte punkty wzorca
               if( mode == CAPTURING && (clock() - prevTimestamp > 200*1e-3*CLOCKS_PER_SEC) )
               {
                   image_points.push_back(pointBuf);
                   prevTimestamp = clock();
                   blinkOutput = true;
               }

               // Rysuj narożniki szachownicy
               drawChessboardCorners( view, s.boardSize, Mat(pointBuf), found );
       }

       //----------------------------- Wyświetl tekst na obrazie ------------------------------------------------

       string msg = (mode == CAPTURING) ? "100/100" :
                     mode == CALIBRATED ? "Calibrated" : "Press 's' to start";
       int baseLine = 0;
       Size textSize = getTextSize(msg, 1, 1, 1, &baseLine);
       Point textOrigin(view.cols - 2*textSize.width - 10, view.rows - 2*baseLine - 10);

       if( mode == CAPTURING )
       {
               msg = format( "%d/%d", (int)image_points.size(), s.nrFrames );
       }

       putText( view, msg, textOrigin, 1, 1, mode == CALIBRATED ?  GREEN : RED);

       if( blinkOutput )
           bitwise_not(view, view);

       //------------------------------ Wyświetlaj obraz i czekaj na klawisz -------------------

       imshow("Image View", view);
       char key = (char)waitKey(true ? 50 : 200);
       if(key == 's')
       {
           mode = CAPTURING;
           image_points.clear();
       }
    }

    /**
     * @brief Run calibration and save parameters
     */
    bool runCalibrationAndSave(Settings& s, Size image_size, Mat& camera_matrix, Mat& dist_coeffs,
                               vector<vector<Point2f> > image_points)
    {

        bool ok = runCalibration(s, camera_matrix, dist_coeffs, image_points);
        cout << (ok ? "Calibration succeeded" : "Calibration failed")
             << endl;

        if (ok)
            saveCameraParams(s, image_size);
        return ok;
    }

    /**
     * @brief saving params to file
     */
     void saveCameraParams( Settings& s, Size& image_size)
    {
        //ścieżka pliku wyjściowego
        s.outputFileName = string(PROJECT_SOURCE_DIR) + "/config/M2_data.xml";

        FileStorage fs( s.outputFileName, FileStorage::WRITE );

        time_t tm;
        time( &tm );
        struct tm *t2 = localtime( &tm );
        char buf[1024];
        strftime( buf, sizeof(buf), "%c", t2 );

        fs << "calibration_time" << buf;

        fs << "image_width" << image_size.width;
        fs << "image_height" << image_size.height;
        fs << "board_width" << s.boardSize.width;
        fs << "board_height" << s.boardSize.height;
        fs << "square_size" << s.squareSize;
        fs << "rotation_matrix" << rotation;
        fs << "translation_matrix" << translation;
    }

    void calcBoardCornerPositions(Size boardSize, float squareSize, vector<Point3f>& corners,
                                         Settings::Pattern patternType /*= Settings::CHESSBOARD*/)
    {
        corners.clear();

        switch(patternType)
        {
        case Settings::CHESSBOARD:
        case Settings::CIRCLES_GRID:
            for( int i = 0; i < boardSize.height; ++i )
                for( int j = 0; j < boardSize.width; ++j )
                    corners.push_back(Point3f(j*squareSize, i*squareSize, 0));
            break;

        case Settings::ASYMMETRIC_CIRCLES_GRID:
            for( int i = 0; i < boardSize.height; i++ )
                for( int j = 0; j < boardSize.width; j++ )
                    corners.push_back(Point3f((2*j + i % 2)*squareSize, i*squareSize, 0));
            break;
        default:
            break;
        }
    }

    bool runCalibration( Settings& s, Mat& camera_matrix, Mat& dist_coeffs,
                                vector<vector<Point2f> > image_points)
    {
        // deklarowanie tymczasowych macierzy rotacji oraz translacji
        Mat rvec, tvec;        

        // wyznaczenie punktów charakterystycznych na obiekcie (wzorcu)
        // w układzie współrzędnych wzorca
        vector<vector<Point3f> > objectPoints(1);
        calcBoardCornerPositions(s.boardSize, s.squareSize, objectPoints[0], s.calibrationPattern);

        objectPoints.resize(image_points.size(),objectPoints[0]);

        // wyliczenie macierzy rotacji i translacji przekształcających
        // punkty z obrazu kamery w odpowiadające im punkty na płaszczyźnie podłogi
        solvePnP(objectPoints.at(0), image_points.at(0), camera_matrix, dist_coeffs, rvec, tvec);

        // zamiana rotacji z postaci minimalnej (wektor) na postać rozszerzoną (macierz)
        Rodrigues (rvec,rotation);
        translation = tvec;

        // sprawdzenie poprawności rozmiaru wyznaczonych macierzy
        bool ok = checkRange(camera_matrix) && checkRange(dist_coeffs);

        return ok;
    }

protected:

    //============================================================================
    //=============================== VARIABLES ==================================
    //============================================================================

    /**
     * @brief image_topic               Sherlock image topic.
     */
    std::string image_topic;

    /**
     * @brief got_image                 Got image flag.
     */
    bool got_image;

    /**
     * @brief sub                       Image subscriber.
     */
    image_transport::Subscriber sub;

    /**
     * @brief timer                     Timer for no-topic message.
     */
    ros::Timer timer;

    Mat image;  // received frame

    Settings calib_settings;  // calibration settings


    //============================================================================
    //============================ SENSORS CALLBACK ==============================
    //============================================================================

    /**
     * @brief timer_callback        Timer callback.
     */
    void timer_callback(const ros::TimerEvent&)
    {
        std::cout << COUT_MODULE_NAME << "no data on " << image_topic
                  << " topic so far." << std::endl;
    }

    /**
     * @brief image_callback        Image callback.
     */
    void image_callback(const sensor_msgs::ImageConstPtr& msg)
    {

        if (!got_image)
        {
            got_image = true;
            timer.stop();
        }

        std::cout << COUT_MODULE_NAME << "got image! " << std::endl;
        image = cv_bridge::toCvShare(msg, "bgr8")->image;
        this->mainCalibFunction(image, calib_settings);
    }
};

static inline void read(const FileNode& node, Settings& x, const Settings& default_value = Settings())
{
    if(node.empty())
        x = default_value;
    else
        x.read(node);
}

static inline void write(FileStorage& fs, const String&, const Settings& s )
{
    s.write(fs);
}


//================================================================================
//================================== MAIN ROUTINE ================================
//================================================================================

int main(int argc, char* argv[])
{
    Settings s;
    const string inputSettingsFile = argc > 1 ? argv[1] : string(PROJECT_SOURCE_DIR) + "/config/config_calibration_2.xml";
    FileStorage fs(inputSettingsFile, FileStorage::READ);
    if (!fs.isOpened())
    {
        cout << COUT_MODULE_NAME << "Could not open the configuration file: \"" << inputSettingsFile << "\"" << endl;
        return -1;
    }
    else
        cout << COUT_MODULE_NAME << " Configuration file opened " << endl;
    fs["Settings"] >> s;
    fs.release();                                         // close Settings file

    if (!s.goodInput)
    {
        cout << COUT_MODULE_NAME << "Invalid input detected. Application stopping. " << endl;
        return -1;
    }

    string f2path = string(PROJECT_SOURCE_DIR) + "/config/M1_data.xml";
    FileStorage fs2(f2path, FileStorage::READ);
    fs2["camera_matrix"] >> camera_matrix;
    cout << COUT_MODULE_NAME << "Camera Matrix: " << camera_matrix << endl;
    fs2["distortion_coefficients"] >> dist_coeffs;
    cout << COUT_MODULE_NAME << "Dist Coeffs: " << dist_coeffs << endl;
    fs2.release();

    // Ros stuff
    ros::init(argc, argv, module_name);
    ros::NodeHandle n("~");

    // Calibration
    CalibrationExtrinsic calib(n,s);

    // Spin!
    ros::spin();

    return 0;
}

