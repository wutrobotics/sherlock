// standard
#include <iostream>

// opencv
#include <opencv2/highgui/highgui.hpp>

// ROS includes
#include <ros/ros.h>
#include <cv_bridge/cv_bridge.h>
#include <image_transport/image_transport.h>

//================================================================================
//================================= COLOR STRINGS ================================
//================================================================================

const std::string module_name = "image_view";

const std::string red("\033[0;31m");
const std::string green("\033[1;32m");
const std::string yellow("\033[1;33m");
const std::string cyan("\033[0;36m");
const std::string magenta("\033[0;35m");
const std::string reset("\033[0m");

#define COUT_MODULE_NAME   green << "sherlock::" << module_name << " : " << reset
#define CERR_MODULE_NAME   "sherlock::" << module_name << " : "

//================================================================================
//================================== GLOBAL VARS =================================
//================================================================================

/**
 * @brief got_image             Got image flag.
 */
bool got_image = false;

/**
 * @brief image_topic           Sherlock image topic.
 */
std::string image_topic;

/**
 * @brief timer                 Timer for no-topic message.
 */
ros::Timer timer;

//================================================================================
//================================= TIMER CALLBACK ===============================
//================================================================================

void timerCallback(const ros::TimerEvent&)
{
    std::cout << COUT_MODULE_NAME << "no data on " << image_topic << " topic so far." << std::endl;
}

//================================================================================
//================================= IMAGE CALLBACK ===============================
//================================================================================

void imageCallback(const sensor_msgs::ImageConstPtr& msg)
{
    if (!got_image)
    {
        cv::namedWindow(module_name);
        cv::startWindowThread();
        got_image = true;
        timer.stop();
    }

    try
    {
        cv::imshow(module_name, cv_bridge::toCvShare(msg, "bgr8")->image);
        cv::waitKey(1);
    }
    catch (cv_bridge::Exception& e)
    {
        std::cerr << CERR_MODULE_NAME << "Could not convert from "
                  << msg->encoding.c_str() << " to 'bgr8'." << std::endl;
        ros::Duration(1).sleep();
    }
}

//================================================================================
//================================== MAIN ROUTINE ================================
//================================================================================

int main(int argc, char* argv[])
{
    ros::init(argc, argv, module_name);
    ros::NodeHandle n("~");

    // Get parent ns
    std::string ns = n.getNamespace();
    std::string parent_ns = ros::names::parentNamespace(ns);
    if (parent_ns == "/") image_topic = "/camera_driver/image";
    else image_topic = parent_ns + "/camera_driver/image";

    // Define subscriber
    image_transport::ImageTransport it(n);
    image_transport::Subscriber sub = it.subscribe(image_topic, 10, imageCallback);

    // Define timer
    timer = n.createTimer(ros::Duration(5), timerCallback, true);

    // Spin!
    ros::spin();

    // Clean up
    if (got_image) cv::destroyWindow(module_name);
    timer.stop();
    return 0;
}
