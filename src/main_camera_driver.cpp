//standard
#include <iostream>
#include <string.h>
#include <math.h>
#include <time.h>
#include <fstream>
#include <ostream>

//pylon
#include <pylon/PylonIncludes.h>
#include <pylon/gige/BaslerGigEInstantCamera.h>

//opencv
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>

//ROS includes
#include <ros/ros.h>
#include <cv_bridge/cv_bridge.h>
#include <image_transport/image_transport.h>

#ifdef PYLON_WIN_BUILD
#include <pylon/PylonGUI.h>
#endif

using namespace Pylon;
using namespace cv;
using namespace std;
using namespace GenApi;

typedef CBaslerGigEInstantCamera Camera_t;
typedef CBaslerGigEGrabResultPtr GrabResultPtr_t;

enum {TRIGGER_OFF = 0, TRIGGER_ON = 1};

//================================================================================
//================================= COLOR STRINGS ================================
//================================================================================

const std::string module_name = "camera_driver";

const std::string red("\033[0;31m");
const std::string green("\033[1;32m");
const std::string yellow("\033[1;33m");
const std::string cyan("\033[0;36m");
const std::string magenta("\033[0;35m");
const std::string reset("\033[0m");

#define COUT_MODULE_NAME   green << "sherlock::" << module_name << " : " << reset
#define CERR_MODULE_NAME   "sherlock::" << module_name << " : "

// ----------------------------------------------------------------------------

double obliczSekundy( clock_t czas )
{
    return static_cast < double >( czas ) / CLOCKS_PER_SEC;
}

//--------------------------------------------------------MAIN-----------------------------------------------------------------
int main(int argc, char* argv[])
{

    Mat image;
    int mode = TRIGGER_OFF;

    //============================================================================
    //=============================== NODE INIT ==================================
    //============================================================================

    ros::init(argc, argv, module_name);
    ros::NodeHandle n("~");
    ros::Rate rate(25); // ilość razy na sekundę
    image_transport::ImageTransport it(n);
    image_transport::Publisher pub_image = it.advertise("image", 1);

    //============================================================================
    //============================= PARAMS PARSING ===============================
    //============================================================================

    string params_acquisition_filepath;

    if(!n.hasParam("mode"))
    {
        cout << COUT_MODULE_NAME << "'mode' parameter required" << endl;
        return -1;
    }

    string param_mode;
    n.getParam("mode", param_mode);
    if (param_mode == "triggered") mode = TRIGGER_ON;
    else if (param_mode == "freerun") mode = TRIGGER_OFF;
    else
    {
        cout << COUT_MODULE_NAME << "'mode' parameter can be only 'triggered' or 'freerun'" << endl;
        return -1;
    }

    if (mode == TRIGGER_ON )
		params_acquisition_filepath = string(PROJECT_SOURCE_DIR) + "/config/camera_params_trigger_on.pfs";
	else
		params_acquisition_filepath = string(PROJECT_SOURCE_DIR) + "/config/camera_params_trigger_off.pfs";

    char Filename[params_acquisition_filepath.size()-1];
    strcpy(Filename, params_acquisition_filepath.c_str());

    ifstream infile(params_acquisition_filepath);
    if (infile.good()) cout << COUT_MODULE_NAME << "Using user defined parameters configuration file" << endl;
    else
    {
        cerr << CERR_MODULE_NAME << "Cannot open parameters configuration file" << endl;
        ros::Duration(0.1).sleep();
        return 0;
    }

    //============================================================================
    // PYLON CODE & ROS IMAGE PUBLISHER
    //============================================================================

    Pylon::PylonAutoInitTerm autoInitTerm;

    CDeviceInfo info;
    info.SetDeviceClass( Camera_t::DeviceClass());
    Camera_t camera( CTlFactory::GetInstance().CreateFirstDevice( info));
    cout << COUT_MODULE_NAME << "Using device " << camera.GetDeviceInfo().GetModelName() <<endl;
    camera.Open();

    CFeaturePersistence::Load(Filename, &camera.GetNodeMap(), true );

    camera.MaxNumBuffer = 10;
    CImageFormatConverter formatConventer;
    formatConventer.OutputPixelFormat = PixelType_BGR8packed;
    CPylonImage pylonImage;
    camera.StartGrabbing(GrabStrategy_LatestImageOnly,GrabLoop_ProvidedByUser);
    CGrabResultPtr ptrGrabResult;


    // --------------------------- Pobieranie obrazu -------------------------------
    while (camera.IsGrabbing() && n.ok())
    {
        cout << "Image grabbed " << endl;

        // czekaj na sygnał wyzwalający
		if(mode == TRIGGER_ON)
        camera.WaitForFrameTriggerReady( 500, TimeoutHandling_ThrowException);

        // pobierz obraz z kamery
        camera.RetrieveResult(5000,ptrGrabResult, TimeoutHandling_ThrowException);

        if(ptrGrabResult->GrabSucceeded())
        {
            formatConventer.Convert(pylonImage, ptrGrabResult);

            // konwersja obrazu do cvMat
            image = cv::Mat(ptrGrabResult->GetHeight(), ptrGrabResult->GetWidth(), CV_8UC3,
                            (uint8_t*) pylonImage.GetBuffer());
            // konwersja obrazu z cvMat do ROS image msg
            sensor_msgs::ImagePtr msg = cv_bridge::CvImage(std_msgs::Header(), "bgr8",
                                                           image).toImageMsg();
            // publikowanie obrazu w ROSie
            pub_image.publish(msg);     // Publikowanie wiadomości
            ros::spinOnce();            // Spin (by wysłać info z publishera do całego rosa)
            rate.sleep();               // Czekamy, by wysyłać z opowiednią częstotliwością
        }
        else
        {
            cout << "Error: " << ptrGrabResult->GetErrorCode() << " " <<
                    ptrGrabResult->GetErrorDescription() << endl;
        }
    }

    //Clear params
    n.deleteParam("mode");

    return 0;
}
