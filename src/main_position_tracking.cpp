// standard
#include <iostream>
#include <string>
#include <iomanip>
#include <string.h>
#include <math.h>

// opencv
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/video/video.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/videoio/videoio.hpp>

// ROS includes
#include <ros/ros.h>
#include <cv_bridge/cv_bridge.h>
#include <image_transport/image_transport.h>
#include <geometry_msgs/Point.h>
#include <tf/transform_broadcaster.h>
#include <tf/transform_listener.h>

using namespace cv;
using namespace std;

//================================================================================
//================================= COLOR STRINGS ================================
//================================================================================

const std::string module_name = "position_tracking";

const std::string red("\033[0;31m");
const std::string green("\033[1;32m");
const std::string yellow("\033[1;33m");
const std::string cyan("\033[0;36m");
const std::string magenta("\033[0;35m");
const std::string reset("\033[0m");

#define COUT_MODULE_NAME   green << "sherlock::" << module_name << " : " << reset
#define CERR_MODULE_NAME   "sherlock::" << module_name << " : "

//================================================================================
//================================== GLOBAL VARS =================================
//================================================================================

// gt = ground_truth

/**
 * @brief image_topic           Sherlock image topic.
 */
std::string image_topic;

/**
 * @brief point3d_pub           Point publisher.
 */
ros::Publisher point3d_pub;

/**
 * @brief got_image             Got image flag.
 */
bool got_image = false;

/**
 * @brief timer                 Timer for no-topic message.
 */
ros::Timer timer;

struct TwoPoints
{
    Point p1; // ośmiokąt
    Point p2; // sześciokąt
};

static TwoPoints base_points, gt_points;
static float angle, X, Y;
static Point octagon_center, hexagon_center;
Mat image, camera_matrix, rotation_matrix, translation_matrix, H_inv;
vector<vector<Point> > octagon, hexagon;
bool base_found = false;
bool detected_both = false;
tf::StampedTransform base_transform_received;
tf::Transform base_transform_to_send;
tf::Transform gt_transform_to_send;
tf::Transform mult_transform_to_send;
tf::Quaternion q;
string robot_name;
double delta_yaw, delta_x, delta_y;
double robot_height;


//================================================================================
//================================= MARKER DETECTION =============================
//================================================================================

// Funkcja do zaokrąglania do jedności
double round(double d)
{
    return floor(d + 0.5);
}

// Funkcja do zamiany float na string
string Convert (float number)
{
    ostringstream buff;
    buff << number;
    return buff.str();
}

// Funkcja wyszukująca wielokąty
static void findPolygon(Mat& image)
{
    hexagon.clear();
    octagon.clear();
    Mat pyr, gray(image.size(),CV_8U), kraw;
    vector<vector<Point> > contours;
    vector<Vec4i> hierarchy;
    vector<Point> approx;
    int area = 700;

    // Usuwanie szumu
    pyrDown(image, pyr, Size(image.cols/2, image.rows/2));
    pyrUp(pyr, pyr, image.size());

    // Przejście na skale szarości i wykrywanie krawędzi
    cvtColor(pyr, gray, CV_BGR2GRAY);
    Canny(gray, kraw, 100, 150, 3);

    imshow(module_name, kraw);

    // Wykrywanie konturów i zapisanie ich na listę
    findContours(kraw, contours, hierarchy, RETR_LIST, CHAIN_APPROX_SIMPLE);

    for( size_t i = 0; i < contours.size(); i++ )
        {
        // Przybliżenie konturu wielokątem
        approxPolyDP(Mat(contours[i]), approx, arcLength(Mat(contours[i]),
                                                         true)*0.02, true);
        // Sprawdzanie ośmiokątów
        if( approx.size() == 8 && isContourConvex(Mat(approx)) &&
                fabs(contourArea(Mat(approx))) > area)
            {                
                octagon.push_back(approx);
            }

        // Sprawdzanie sześciokątów
        if( approx.size() == 6 && isContourConvex(Mat(approx)) &&
                fabs(contourArea(Mat(approx))) > area)
            {
                hexagon.push_back(approx);
            }
        }
}

// Funkcja wykrywająca środek markera
static void calculateCenter(Mat& image)
{
    vector<Moments> mu( octagon.size() );
    vector<Point2f> mc( octagon.size() );
    vector<Moments> mu1( hexagon.size() );
    vector<Point2f> mc1( hexagon.size() );
    string text;
    int o = 20;
    detected_both = false;

    for( size_t i = 0; i < octagon.size(); i+=2 )
    {
        // Liczenie momentu
        mu[i] = moments( octagon[i], false );
        // Liczenie środka ciężkości
        mc[i] = Point2f( mu[i].m10/mu[i].m00 , mu[i].m01/mu[i].m00 );
    }

    for( size_t i = 0; i < hexagon.size(); i+=2 )
    {
        // Liczenie momentu
        mu1[i] = moments( hexagon[i], false );
        // Liczenie środka ciężkości
        mc1[i] = Point2f( mu1[i].m10/mu1[i].m00 , mu1[i].m01/mu1[i].m00 );
    }

    for( size_t i = 2; i < octagon.size(); i+=2 )
    {
        // Jeśli środki ciężkości 2 sąsiednich figur pokrywają się
        if (mc[i].x < mc[i-2].x+o && mc[i].x > mc[i-2].x-o && mc[i].y <
                mc[i-2].y+o && mc[i].y > mc[i-2].y-o)
        {
            int n = (int)octagon[i].size();
            const Point* p0 = &octagon[i][0];

            // Rysuj obramowanie i przekątne markera
            polylines(image, &p0, &n, 1, true, Scalar(0,255,0), 2, LINE_AA);

            // Rysuj środek markery i jego współrzędne
            circle( image, mc[i], 4, Scalar(255,0,0), -1, LINE_AA, 0 );
            text = "("+Convert(round(mc[i].x))+", "+Convert(round(mc[i].y))+")";
            putText(image, text, Point(mc[i].x-60, mc[i].y-50), FONT_HERSHEY_SIMPLEX,
                    0.7, Scalar(0,0,255), 2, LINE_AA, false);
        }
        for( size_t j = 2; j < hexagon.size(); j+=2 )
        {
            // Jeśli środki ciężkości 2 sąsiednich figur pokrywają się
            if (mc1[j].x < mc1[j-2].x+o && mc1[j].x > mc1[j-2].x-o && mc1[j].y <
                    mc1[j-2].y+o && mc1[j].y > mc1[j-2].y-o)
            {
                int k = (int)hexagon[j].size();
                const Point* p01 = &hexagon[j][0];

                // Rysuj obramowanie i przekątne markera
                polylines(image, &p01, &k, 1, true, Scalar(0,255,0), 2, LINE_AA);

                // Rysuj środek markera i jego współrzędne
                circle( image, mc1[j], 4, Scalar(255,0,0), -1, LINE_AA, 0 );
                text = "("+Convert(round(mc1[j].x))+", "+Convert(round(mc1[j].y))+")";
                putText(image, text, Point(mc1[j].x-60, mc1[j].y-50),FONT_HERSHEY_SIMPLEX,
                        0.7, Scalar(0,0,255), 2, LINE_AA, false);
                line(image, octagon_center, hexagon_center, Scalar(0,0,255),2, LINE_AA);

                // Zapamiętanie środków markerów
                hexagon_center = mc1[j];
                octagon_center = mc[i];
                detected_both = true;
            }
        }
    }
}


//================================================================================
//================================= POINTS TRANSFORM =============================
//================================================================================

/**
 * @brief transformToPlane        Transformacja z układu kamery do układu wzorca
                                  H = K * [R1, R2, t]
                                  P = H_inv *p *t3
 */
static Point transformToPlane(Point point)
{
    Mat p = Mat::zeros(3,1,CV_64F);  //punkt na obrazie

    p.at<double>(0,0) = point.x;
    p.at<double>(1,0) = point.y;
    p.at<double>(2,0) = 1;

    Mat P = H_inv * p; //punkt na podłodze

    // normalizacja
    Point new_marker_center;
    new_marker_center.x = P.at<double>(0,0)* translation_matrix.at<double>(2,0);
    new_marker_center.y = P.at<double>(1,0)* translation_matrix.at<double>(2,0);

    return new_marker_center;
}

/**
 * @brief transformToBase            Transformacja z układu wzorca do układu sherlock_base
                                     X =  Cos x - Sin y + Tx
                                     Y = -Sin x - Cos y + Ty
 */
static Point transformToBase(Point point)
{
    Point new_marker_center;

    new_marker_center.x = point.x * cos(delta_yaw) - point.y * sin(delta_yaw) + delta_x;
    new_marker_center.y = - point.x * sin(delta_yaw) - point.y * cos(delta_yaw) + delta_y;

    return new_marker_center;
}

//================================================================================
//================================= ORIGINS TRANSFORM ============================
//================================================================================

/**
 * @brief findOriginTransform          Funkcja licząca przesunięcie oraz obrót znacznika
 *                                      w układzie sherlock_base
 */
static void findOriginTransform ()
{
    // policz współczynnik slope prostych i zamień na kąt
    float angle1 = atan2(base_points.p1.y - base_points.p2.y,
                         base_points.p1.x - base_points.p2.x);
    float angle2 = atan2(gt_points.p1.y - gt_points.p2.y,
                         gt_points.p1.x - gt_points.p2.x);

    cout << endl << "ANGLE1:  " << angle1 << endl;
    cout << endl << "ANGLE2:  " << angle2 << endl;

    // wyznacz kąt między prostymi oraz przesunięcie środków ośmiokątów
    angle = angle1-angle2;
    X = (base_points.p1.x - gt_points.p1.x) ;
    Y = (base_points.p1.y - gt_points.p1.y) ;

    // zamiana na metry
    X = X * 0.001;
    Y = Y * 0.001;

    // drukuj pozycję i orientację robota
   //  cout << "X:  " << X << endl;
   // cout << "Y:  " << Y << endl;
   // cout << "angle:  " << angle << endl;
}

//================================================================================
//================================= BASE TF ======================================
//================================================================================

static void baseTransform(Point octagon_center, Point hexagon_center)
{

    tf::TransformListener listener;
    ros::Rate rate(50.0);
    while (1)
    {
      try
      {
        // zapamiętaj transoformację stałą sherlock_base -> odom
        listener.lookupTransform(robot_name + "/odom", robot_name +"/base_link", ros::Time(0),
                                    base_transform_received);

        base_transform_to_send.setOrigin( tf::Vector3(
                                            base_transform_received.getOrigin()));

        base_transform_to_send.setRotation( tf::Quaternion(
                                            base_transform_received.getRotation()));
        break;
      }
      catch (tf::TransformException &ex)
      {
        ROS_ERROR("%s",ex.what());
        ros::Duration(1).sleep();
      }
      rate.sleep();
    }

    base_points.p1 = transformToPlane(octagon_center);
    base_points.p2 = transformToPlane(hexagon_center);

    // zapamiętanie kąta i przesunięcia do transformacji szachownica -> sherlock_base
    delta_x = base_points.p1.x;
    delta_y = base_points.p1.y;
    float angle1 = atan2(base_points.p1.y - base_points.p2.y,
                         base_points.p1.x - base_points.p2.x);
    delta_yaw = -angle1;

    base_points.p1 = transformToBase(base_points.p1);
    base_points.p2 = transformToBase(base_points.p2);

    cout << "BASEE:   " << base_points.p1.x << "  " << base_points.p1.y << endl;
}

//================================================================================
//================================= TIMER CALLBACK ===============================
//================================================================================

void timer_callback(const ros::TimerEvent&)
{
    std::cout << COUT_MODULE_NAME << "no data on " << image_topic << " topic so far." << std::endl;
}

//================================================================================
//================================= IMAGE CALLBACK ===============================
//================================================================================

void imageCallback(const sensor_msgs::ImageConstPtr& msg)
{
    if (!got_image)
    {
        cv::namedWindow(module_name);
        cv::startWindowThread();
        got_image = true;
        timer.stop();
    }

    try
    {
        image = cv_bridge::toCvShare(msg, "bgr8")->image;       // konwersja na cvMat
    }
    catch (cv_bridge::Exception& e)
    {
        std::cerr << CERR_MODULE_NAME << "Could not convert from "
                  << msg->encoding.c_str() << " to 'bgr8'." << std::endl;
        ros::Duration(1).sleep();
        return;
    }

    // znajdz krawędzie i wielokąty
    findPolygon(image);

    // znajdz środki markera
    calculateCenter(image);

    // jeśli jest to pierwsze wykrycie to zapamiętaj ten układ jako bazowy - sherlock_base
    if(base_found == false && detected_both == true)
    {
        baseTransform(octagon_center,hexagon_center);
        base_found = true;
    }

    // transformacja środków markerów ze współrzędnych obrazu do współrzędnych wzorca
    gt_points.p1 = transformToPlane(octagon_center);
    gt_points.p2 = transformToPlane(hexagon_center);
    // transformacja środków markerów ze współrzędnych wzorca do współrzędnych sherlock_base
    gt_points.p1 = transformToBase(gt_points.p1);
    gt_points.p2 = transformToBase(gt_points.p2);

    // znajdź kąt oraz przesunięcie względem układu bazowego
    findOriginTransform();

    // zamień kąt na Quaterion
    q.setRPY(0, 0, -angle);

    // zdefiniuj transformację na podstawie układów współrzędnych wyznaczonych przez znaczniki
    gt_transform_to_send.setOrigin( tf::Vector3(X, Y, 0.0));
    gt_transform_to_send.setRotation(q);

    // wyślij transformacje do ROSa
    static tf::TransformBroadcaster base_transform_br;
    static tf::TransformBroadcaster gt_transform_br;
    //static tf::TransformBroadcaster mult_transform_br;
    base_transform_br.sendTransform(tf::StampedTransform(base_transform_to_send, ros::Time::now(),
                                                         robot_name + "/odom",
                                                         robot_name + "/sherlock_base"));
    gt_transform_br.sendTransform(tf::StampedTransform(gt_transform_to_send, ros::Time::now(),
                                                       robot_name + "/sherlock_base",
                                                       robot_name +"/sherlock_gt"));
    /*
    mult_transform_to_send.mult(base_transform_to_send, gt_transform_to_send);
    mult_transform_br.sendTransform(tf::StampedTransform(mult_transform_to_send, ros::Time::now(),
                                                          robot_name + "/odom", robot_name +"/sherlock_gt"));  */
    cv::imshow(module_name, image);
    cv::waitKey(1);
}

//================================================================================
//================================== MAIN ROUTINE ================================
//================================================================================

int main(int argc, char* argv[])
{
    string f1path = string(PROJECT_SOURCE_DIR) + "/config/M1_data.xml";
    string f2path = string(PROJECT_SOURCE_DIR) + "/config/M2_data.xml";

    // Read M1 from file
    FileStorage fs1(f1path, FileStorage::READ);
    fs1["camera_matrix"] >> camera_matrix;
    fs1.release();
    cout << COUT_MODULE_NAME << endl << "Camera Matrix: " << endl << camera_matrix << endl;

    // Read M2 from file
    FileStorage fs2(f2path, FileStorage::READ);
    fs2["rotation_matrix"] >> rotation_matrix;
    cout << COUT_MODULE_NAME << endl << "Rotation Matrix: " << endl << rotation_matrix << endl;
    fs2["translation_matrix"] >> translation_matrix;
    cout << COUT_MODULE_NAME << endl << "Translation Matrix: " << endl << translation_matrix << endl;
    fs2.release();

    // ROS stuff
    ros::init(argc, argv, module_name);
    ros::NodeHandle n("~");

    // ROS param parsing
    if(!n.hasParam("robot"))
    {
        cout << COUT_MODULE_NAME << "No robot name defined" << endl;
        cout << COUT_MODULE_NAME << "Using default robot name - batman" << endl;
        robot_name = "batman";
    }
    else
    {
        n.getParam("robot", robot_name);
    }

    if(!n.hasParam("h"))
    {
        cout << COUT_MODULE_NAME << "No robot height defined" << endl;
        cout << COUT_MODULE_NAME << "Using default height for batman - 273mm" << endl;
        robot_height = 273;
    }
    else
    {
        n.getParam("h", robot_height);
    }

    // Get camera rotation angles
    double r,p,y;
    tf::Matrix3x3 rot = tf::Matrix3x3(
                   rotation_matrix.at<double>(0,0),rotation_matrix.at<double>(0,1),rotation_matrix.at<double>(0,2),
                   rotation_matrix.at<double>(1,0),rotation_matrix.at<double>(1,1),rotation_matrix.at<double>(1,2),
                   rotation_matrix.at<double>(2,0),rotation_matrix.at<double>(2,1),rotation_matrix.at<double>(2,2));

    rot.getRPY(r,p,y);

    cout << "katy kamery: " << r *(180/3.14)<< "  " << p *(180/3.14) << "  " << y *(180/3.14)<< endl;

    // Normalize robot height
    robot_height = robot_height / cos(p);
    translation_matrix.at<double>(2,0) = translation_matrix.at<double>(2,0) + robot_height;

    // Create Homogenous Mat
    Mat H = Mat::zeros(3,3,CV_64F);
    Mat pom = Mat::zeros(3,3,CV_64F);
    H_inv = Mat::zeros(3,3,CV_64F);

    pom.at<double>(0,1) = rotation_matrix.at<double>(0,0);
    pom.at<double>(1,1) = rotation_matrix.at<double>(1,0);
    pom.at<double>(2,1) = rotation_matrix.at<double>(2,0);

    pom.at<double>(0,1) = rotation_matrix.at<double>(0,1);
    pom.at<double>(1,1) = rotation_matrix.at<double>(1,1);
    pom.at<double>(2,1) = rotation_matrix.at<double>(2,1);

    pom.at<double>(0,2) = translation_matrix.at<double>(0,0);
    pom.at<double>(1,2) = translation_matrix.at<double>(1,0);
    pom.at<double>(2,2) = translation_matrix.at<double>(2,0);

    H = camera_matrix * pom;  //macierz homografii H
    H_inv = H.inv();

    // Get parent ns
    std::string ns = n.getNamespace();
    std::string parent_ns = ros::names::parentNamespace(ns);
    if (parent_ns == "/") image_topic = "/camera_driver/image";
    else image_topic = parent_ns + "/camera_driver/image";

    // Define subscriber
    image_transport::ImageTransport it(n);
    image_transport::Subscriber sub = it.subscribe(image_topic, 10, imageCallback);

    // Define timer
    timer = n.createTimer(ros::Duration(5), timer_callback, true);

    // Spin
     ros::spin();

    // Clean up
    if (got_image) cv::destroyWindow(module_name);
    timer.stop();
    //Clear params
    n.deleteParam("robot");
    n.deleteParam("h");
    return 0;
}
