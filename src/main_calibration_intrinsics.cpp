// standard
#include <iostream>
#include <iomanip>
#include <math.h>
#include <sstream>
#include <string.h>

// opencv
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/core.hpp>
#include <opencv2/core/utility.hpp>
#include <opencv2/imgproc.hpp>
#include <opencv2/calib3d.hpp>
#include <opencv2/imgcodecs.hpp>
#include <opencv2/videoio.hpp>
#include <opencv2/highgui.hpp>

// ROS includes
#include <ros/ros.h>
#include <cv_bridge/cv_bridge.h>
#include <image_transport/image_transport.h>

using namespace cv;
using namespace std;

enum { DETECTION = 0, CAPTURING = 1, CALIBRATED = 2 };

Mat sub_image;
vector<vector<Point2f> > image_points;
Mat camera_matrix, dist_coeffs;
Size image_size;
int mode = DETECTION;
clock_t prevTimestamp = 0;
const Scalar RED(0,0,255), GREEN(0,255,0);
const char ESC_KEY = 27;

//================================================================================
//================================= COLOR STRINGS ================================
//================================================================================

const std::string module_name = "calibration_intrinsics";

const std::string red("\033[0;31m");
const std::string green("\033[1;32m");
const std::string yellow("\033[1;33m");
const std::string cyan("\033[0;36m");
const std::string magenta("\033[0;35m");
const std::string reset("\033[0m");

#define COUT_MODULE_NAME   green << "sherlock::" << module_name << " : " << reset
#define CERR_MODULE_NAME   "sherlock::" << module_name << " : "

//================================================================================
//=============================== SETTINGS CLASS ==============================
//================================================================================

class Settings
{
public:
    Settings() : goodInput(false) {}
    enum Pattern { NOT_EXISTING, CHESSBOARD, CIRCLES_GRID, ASYMMETRIC_CIRCLES_GRID};

    void write(FileStorage& fs) const
    {
        fs << "{"
                  << "BoardSize_Width"  << boardSize.width
                  << "BoardSize_Height" << boardSize.height
                  << "Square_Size"         << squareSize
                  << "Calibrate_Pattern" << patternToUse
                  << "Calibrate_NrOfFrameToUse" << nrFrames
                  << "Calibrate_FixAspectRatio" << aspectRatio
                  << "Calibrate_AssumeZeroTangentialDistortion" << calibZeroTangentDist
                  << "Calibrate_FixPrincipalPointAtTheCenter" << calibFixPrincipalPoint
                  << "Write_DetectedFeaturePoints" << writePoints
                  << "Write_outputFileName"  << outputFileName
                  << "Show_UndistortedImage" << showUndistorsed
                  << "Input_FlipAroundHorizontalAxis" << flipVertical
                  << "Input_Delay" << delay
           << "}";
    }
    void read(const FileNode& node)
    {
        node["BoardSize_Width" ] >> boardSize.width;
        node["BoardSize_Height"] >> boardSize.height;
        node["Calibrate_Pattern"] >> patternToUse;
        node["Square_Size"]  >> squareSize;
        node["Calibrate_NrOfFrameToUse"] >> nrFrames;
        node["Calibrate_FixAspectRatio"] >> aspectRatio;
        node["Write_DetectedFeaturePoints"] >> writePoints;
        node["Write_outputFileName"] >> outputFileName;
        node["Calibrate_AssumeZeroTangentialDistortion"] >> calibZeroTangentDist;
        node["Calibrate_FixPrincipalPointAtTheCenter"] >> calibFixPrincipalPoint;
        node["Input_FlipAroundHorizontalAxis"] >> flipVertical;
        node["Show_UndistortedImage"] >> showUndistorsed;
        node["Input_Delay"] >> delay;
        validate();
    }
    void validate()
    {
        goodInput = true;
        if (boardSize.width <= 0 || boardSize.height <= 0)
        {
            cerr << "Invalid Board size: " << boardSize.width << " " << boardSize.height << endl;
            goodInput = false;
        }
        if (squareSize <= 10e-6)
        {
            cerr << "Invalid square size " << squareSize << endl;
            goodInput = false;
        }
        if (nrFrames <= 0)
        {
            cerr << "Invalid number of frames " << nrFrames << endl;
            goodInput = false;
        }

        flag = CALIB_FIX_K4 | CALIB_FIX_K5;
        if(calibFixPrincipalPoint) flag |= CALIB_FIX_PRINCIPAL_POINT;
        if(calibZeroTangentDist)   flag |= CALIB_ZERO_TANGENT_DIST;
        if(aspectRatio)            flag |= CALIB_FIX_ASPECT_RATIO;

        calibrationPattern = NOT_EXISTING;
        if (!patternToUse.compare("CHESSBOARD")) calibrationPattern = CHESSBOARD;
        if (!patternToUse.compare("CIRCLES_GRID")) calibrationPattern = CIRCLES_GRID;
        if (!patternToUse.compare("ASYMMETRIC_CIRCLES_GRID")) calibrationPattern = ASYMMETRIC_CIRCLES_GRID;
        if (calibrationPattern == NOT_EXISTING)
        {
            cerr << " Camera calibration mode does not exist: " << patternToUse << endl;
            goodInput = false;
        }
        atImageList = 0;
    }

public:
    Size boardSize;              // The size of the board -> Number of items by width and height
    Pattern calibrationPattern;  // One of the Chessboard, circles, or asymmetric circle pattern
    float squareSize;            // The size of a square in your defined unit (point, millimeter,etc).
    int nrFrames;                // The number of frames to use from the input for calibration
    float aspectRatio;           // The aspect ratio
    int delay;                   // In case of a video input
    bool writePoints;            // Write detected feature points
    bool calibZeroTangentDist;   // Assume zero tangential distortion
    bool calibFixPrincipalPoint; // Fix the principal point at the center
    bool flipVertical;           // Flip the captured images around the horizontal axis
    string outputFileName;       // The name of the file where to write
    bool showUndistorsed;        // Show undistorted images after calibration
    string input;                // The input ->

    size_t atImageList;
    bool goodInput;
    int flag;

private:
    string patternToUse;
};

//================================================================================
//=============================== CALIBRATION CLASS ===============================
//================================================================================
class CalibrationIntrinsics
{

public:

    //============================================================================
    //============================= MAIN METHODS =================================
    //============================================================================

    /**
     * @brief CalibrationIntrinsics     The class constructor.
     * @param n                         Ros node handle object.
     * @param s                         Calibration Settings read from file.
     */
    CalibrationIntrinsics(ros::NodeHandle& n, Settings& s)
    {
        // Get parent ns
        std::string ns = n.getNamespace();
        std::string parent_ns = ros::names::parentNamespace(ns);
        if (parent_ns == "/") image_topic = "/camera_driver/image";
        else image_topic = parent_ns + "/camera_driver/image";

        // Subscriber
        image_transport::ImageTransport it(n);
        sub = it.subscribe(image_topic, 10, &CalibrationIntrinsics::image_callback, this);

        // Timer
        timer = n.createTimer(ros::Duration(5), &CalibrationIntrinsics::timer_callback, this, true);

        // Haven't image yet
        got_image = false;

        calib_settings = s;
    }

    /**
     * @brief ~CalibrationIntrinsics    Destructor.
     */
    ~CalibrationIntrinsics()
    {
        timer.stop();
    }

    /**
     * @brief main_calib_function     Calibration Loop
     */
    void mainCalibFunction (Mat view, Settings& s)
    {
        bool blinkOutput = false;

       //-----  Jeśli brak obrazu lub pobrano wszystkie klatki to uruchom kalibracje  -------------
       if( mode == CAPTURING && image_points.size() >= (size_t)s.nrFrames )
       {
         if( runCalibrationAndSave(s, image_size,  camera_matrix, dist_coeffs, image_points))
             mode = CALIBRATED;
         else
             mode = DETECTION;
       }

       image_size = view.size();  // Rozmiar pobranej klatki
       if( s.flipVertical )    flip( view, view, 0 );

       // -------------------------- Znajdź wzorzec --------------------------
       vector<Point2f> pointBuf;

       bool found;

       int chessBoardFlags = CALIB_CB_ADAPTIVE_THRESH | CALIB_CB_NORMALIZE_IMAGE;
           chessBoardFlags |= CALIB_CB_FAST_CHECK;

       switch( s.calibrationPattern ) // Znajdź charakterystyczne punkty na obrazie
       {
       case Settings::CHESSBOARD:
           found = findChessboardCorners( view, s.boardSize, pointBuf, chessBoardFlags);
           break;
       case Settings::CIRCLES_GRID:
           found = findCirclesGrid( view, s.boardSize, pointBuf );
           break;
       case Settings::ASYMMETRIC_CIRCLES_GRID:
           found = findCirclesGrid( view, s.boardSize, pointBuf, CALIB_CB_ASYMMETRIC_GRID );
           break;
       default:
           found = false;
           break;
       }

       // ------------------------ Jeśli znaleziono ------------------------------------
       if (found)
       {
               // Przejście na skalę szarności w celu lepszego wykrycia wzorca
               if( s.calibrationPattern == Settings::CHESSBOARD)
               {
                   Mat viewGray;
                   cvtColor(view, viewGray, COLOR_BGR2GRAY);
                   cornerSubPix( viewGray, pointBuf, Size(11,11),
                       Size(-1,-1), TermCriteria( TermCriteria::EPS+TermCriteria::COUNT, 30, 0.1 ));
               }

               // Zapamiętaj wykryte punkty charakterystyczne wzorca
               if( mode == CAPTURING && (clock() - prevTimestamp > s.delay*1e-3*CLOCKS_PER_SEC) )
               {
                   image_points.push_back(pointBuf);
                   prevTimestamp = clock();
                   blinkOutput = true;
               }

               // Rysuj narożniki szachownicy
               drawChessboardCorners( view, s.boardSize, Mat(pointBuf), found );
       }

       //----------------------------- Wyświetl tekst na obrazie --------------------------

       string msg = (mode == CAPTURING) ? "100/100" :
                     mode == CALIBRATED ? "Calibrated" : "Press 's' to start";
       int baseLine = 0;
       Size textSize = getTextSize(msg, 1, 1, 1, &baseLine);
       Point textOrigin(view.cols - 2*textSize.width - 10, view.rows - 2*baseLine - 10);

       if( mode == CAPTURING )
       {
           if(s.showUndistorsed)
               msg = format( "%d/%d Undist", (int)image_points.size(), s.nrFrames );
           else
               msg = format( "%d/%d", (int)image_points.size(), s.nrFrames );
       }

       putText( view, msg, textOrigin, 1, 1, mode == CALIBRATED ?  GREEN : RED);

       if( blinkOutput )
           bitwise_not(view, view);

       //------------------------- Pokaż obraz po usunięciu dystorsji ------------------------------

       if( mode == CALIBRATED && s.showUndistorsed )
       {
           Mat temp = view.clone();
           undistort(temp, view, camera_matrix, dist_coeffs);
       }

       //---------------------------- Wyświetlaj obraz i czekaj na klawisz -------------------

       imshow("Image View", view);
       char key = (char)waitKey(true ? 50 : s.delay);

       if( key == 'u' && mode == CALIBRATED )
          s.showUndistorsed = !s.showUndistorsed;

       if(key == 's')
       {
           mode = CAPTURING;
           image_points.clear();
       }
    }

    bool runCalibrationAndSave(Settings& s, Size image_size, Mat& camera_matrix, Mat& dist_coeffs,
                               vector<vector<Point2f> > image_points)
    {
        vector<Mat> rvecs, tvecs;
        vector<float> reprojErrs;
        double totalAvgErr = 0;

        bool ok = runCalibration(s, image_size, camera_matrix, dist_coeffs, image_points, rvecs, tvecs, reprojErrs,
                                 totalAvgErr);
        cout << (ok ? "Calibration succeeded" : "Calibration failed")
             << ". avg re projection error = " << totalAvgErr << endl;

        if (ok)
            saveCameraParams(s, image_size, camera_matrix, dist_coeffs, rvecs, tvecs, reprojErrs, image_points,
                             totalAvgErr);
        return ok;
    }


     void saveCameraParams( Settings& s, Size& image_size, Mat& camera_matrix, Mat& dist_coeffs,
                                  const vector<Mat>& rvecs, const vector<Mat>& tvecs,
                                  const vector<float>& reprojErrs, const vector<vector<Point2f> >& image_points,
                                  double totalAvgErr )
    {
        s.outputFileName = string(PROJECT_SOURCE_DIR) + "/config/M1_data.xml";

        FileStorage fs( s.outputFileName, FileStorage::WRITE );

        time_t tm;
        time( &tm );
        struct tm *t2 = localtime( &tm );
        char buf[1024];
        strftime( buf, sizeof(buf), "%c", t2 );

        fs << "calibration_time" << buf;

        if( !rvecs.empty() || !reprojErrs.empty() )
            fs << "nr_of_frames" << (int)std::max(rvecs.size(), reprojErrs.size());
        fs << "image_width" << image_size.width;
        fs << "image_height" << image_size.height;
        fs << "board_width" << s.boardSize.width;
        fs << "board_height" << s.boardSize.height;
        fs << "square_size" << s.squareSize;

        if( s.flag & CALIB_FIX_ASPECT_RATIO )
            fs << "fix_aspect_ratio" << s.aspectRatio;

        if (s.flag)
        {          
                sprintf(buf, "flags:%s%s%s%s",
                         s.flag & CALIB_USE_INTRINSIC_GUESS ? " +use_intrinsic_guess" : "",
                         s.flag & CALIB_FIX_ASPECT_RATIO ? " +fix_aspectRatio" : "",
                         s.flag & CALIB_FIX_PRINCIPAL_POINT ? " +fix_principal_point" : "",
                         s.flag & CALIB_ZERO_TANGENT_DIST ? " +zero_tangent_dist" : "");
                 cvWriteComment(*fs, buf, 0);
        }

        fs << "flags" << s.flag;

        fs << "camera_matrix" << camera_matrix;
        fs << "distortion_coefficients" << dist_coeffs;

        fs << "avg_reprojection_error" << totalAvgErr;
        if (!reprojErrs.empty())
            fs << "per_view_reprojection_errors" << Mat(reprojErrs);

        if(s.writePoints && !image_points.empty() )
        {
            Mat imagePtMat((int)image_points.size(), (int)image_points[0].size(), CV_32FC2);
            for( size_t i = 0; i < image_points.size(); i++ )
            {
                Mat r = imagePtMat.row(int(i)).reshape(2, imagePtMat.cols);
                Mat imgpti(image_points[i]);
                imgpti.copyTo(r);
            }
            fs << "image_points" << imagePtMat;
        }
    }

    double computeReprojectionErrors( const vector<vector<Point3f> >& objectPoints,
                                             const vector<vector<Point2f> >& image_points,
                                             const vector<Mat>& rvecs, const vector<Mat>& tvecs,
                                             const Mat& camera_matrix , const Mat& dist_coeffs,
                                             vector<float>& perViewErrors)
    {
        vector<Point2f> image_points2;
        size_t totalPoints = 0;
        double totalErr = 0, err;
        perViewErrors.resize(objectPoints.size());

        for(size_t i = 0; i < objectPoints.size(); ++i )
        {
            projectPoints(objectPoints[i], rvecs[i], tvecs[i], camera_matrix, dist_coeffs, image_points2);
            err = norm(image_points[i], image_points2, NORM_L2);

            size_t n = objectPoints[i].size();
            perViewErrors[i] = (float) std::sqrt(err*err/n);
            totalErr        += err*err;
            totalPoints     += n;
        }

        return std::sqrt(totalErr/totalPoints);
    }


    void calcBoardCornerPositions(Size boardSize, float squareSize, vector<Point3f>& corners,
                                         Settings::Pattern patternType /*= Settings::CHESSBOARD*/)
    {
        corners.clear();

        switch(patternType)
        {
        case Settings::CHESSBOARD:
        case Settings::CIRCLES_GRID:
            for( int i = 0; i < boardSize.height; ++i )
                for( int j = 0; j < boardSize.width; ++j )
                    corners.push_back(Point3f(j*squareSize, i*squareSize, 0));
            break;

        case Settings::ASYMMETRIC_CIRCLES_GRID:
            for( int i = 0; i < boardSize.height; i++ )
                for( int j = 0; j < boardSize.width; j++ )
                    corners.push_back(Point3f((2*j + i % 2)*squareSize, i*squareSize, 0));
            break;
        default:
            break;
        }
    }

    bool runCalibration( Settings& s, Size& image_size, Mat& camera_matrix, Mat& dist_coeffs,
                        vector<vector<Point2f> > image_points, vector<Mat>& rvecs,
                        vector<Mat>& tvecs, vector<float>& reprojErrs,  double& totalAvgErr)
    {
        // deklarowanie macierzy wewnętrznej oraz wektora zniekształceń
        camera_matrix = Mat::eye(3, 3, CV_64F);
        dist_coeffs = Mat::zeros(8, 1, CV_64F);

        // wyznaczenie punktów charakterystycznych na obiekcie (wzorcu)
        // w układzie współrzędnych wzorca
        vector<vector<Point3f> > objectPoints(1);
        calcBoardCornerPositions(s.boardSize, s.squareSize, objectPoints[0], s.calibrationPattern);

        // powielenie wektora punktów charakterystycnych obiektu
        // dla każdego wektora punktów wykrytych na obrazie
        objectPoints.resize(image_points.size(),objectPoints[0]);

        // przeprowadzenie kalibracji - wyznaczenie macierzy wewnętrznej
        double rms;
        rms = calibrateCamera(objectPoints, image_points, image_size, camera_matrix, dist_coeffs,
                              rvecs, tvecs, s.flag);
        cout << "Re-projection error reported by calibrateCamera: "<< rms << endl;

        // sprawdzenie poprawności rozmiaru wyznaczonych macierzy
        bool ok = checkRange(camera_matrix) && checkRange(dist_coeffs);
        totalAvgErr = computeReprojectionErrors(objectPoints, image_points, rvecs, tvecs,
                                                camera_matrix, dist_coeffs, reprojErrs);
        return ok;
    }


protected:

    //============================================================================
    //=============================== VARIABLES ==================================
    //============================================================================

    /**
     * @brief image_topic               Sherlock image topic.
     */
    std::string image_topic;

    /**
     * @brief got_image                 Got image flag.
     */
    bool got_image;

    /**
     * @brief sub                       Image subscriber.
     */
    image_transport::Subscriber sub;

    /**
     * @brief timer                     Timer for no-topic message.
     */
    ros::Timer timer;

    Mat image;  // received frame

    Settings calib_settings;  // calibration settings


    //============================================================================
    //============================ SENSORS CALLBACK ==============================
    //============================================================================

    /**
     * @brief timer_callback        Timer callback.
     */
    void timer_callback(const ros::TimerEvent&)
    {
        std::cout << COUT_MODULE_NAME << "no data on " << image_topic
                  << " topic so far." << std::endl;
    }

    /**
     * @brief image_callback        Image callback.
     */
    void image_callback(const sensor_msgs::ImageConstPtr& msg)
    {

        if (!got_image)
        {
            got_image = true;
            timer.stop();
        }

        std::cout << COUT_MODULE_NAME << "got image! " << std::endl;
        image = cv_bridge::toCvShare(msg, "bgr8")->image;
        this->mainCalibFunction(image, calib_settings);
    }
};

static inline void read(const FileNode& node, Settings& x, const Settings& default_value = Settings())
{
    if(node.empty())
        x = default_value;
    else
        x.read(node);
}

static inline void write(FileStorage& fs, const String&, const Settings& s )
{
    s.write(fs);
}


//================================================================================
//================================== MAIN ROUTINE ================================
//================================================================================

int main(int argc, char* argv[])
{
    Settings s;
    const string inputSettingsFile = argc > 1 ? argv[1] : string(PROJECT_SOURCE_DIR) + "/config/config_calibration_1.xml";
    FileStorage fs(inputSettingsFile, FileStorage::READ);
    if (!fs.isOpened())
    {
        cout << COUT_MODULE_NAME << "Could not open the configuration file: \"" << inputSettingsFile << "\"" << endl;
        return -1;
    }
    else
        cout << COUT_MODULE_NAME << " Configuration file opened " << endl;
    fs["Settings"] >> s;
    fs.release();                                         // close Settings file

    if (!s.goodInput)
    {
        cout << COUT_MODULE_NAME << "Invalid input detected. Application stopping. " << endl;
        return -1;
    }
    
    // Ros stuff
    ros::init(argc, argv, module_name);
    ros::NodeHandle n("~");

    // Calibration
    CalibrationIntrinsics calib(n,s);

    // Spin!
    ros::spin();

    return 0;
}

